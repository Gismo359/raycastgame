CXXFLAGS += $(shell pkg-config --cflags --libs sdl2)
CXXFLAGS += -Iinclude

default: debug profile release

debug:
	$(CXX) -std=c++17 -g -m64 -o bin/debug/main.exe src/main.cpp $(CXXFLAGS)

profile:
	$(CXX) -std=c++17 -Ofast -march=native -flto -m64 -o bin/profile/main.exe src/main.cpp $(CXXFLAGS)

release:
	$(CXX) -std=c++17 -s -Ofast -march=native -flto -m64 -o bin/release/main.exe src/main.cpp $(CXXFLAGS)
