#pragma once

#include <algorithm>

#include "Assert.hpp"
#include "Operators.hpp"
#include "Row.hpp"
#include "Shape2D.hpp"

template <typename T>
struct Array2D;

template <typename T>
struct Slice2D
{
public:
    template <typename U>
    struct Iterator
    {
    public:
        using iterator_category = std::random_access_iterator_tag;
        using value_type        = Row<U>;
        using difference_type   = int64_t;
        using pointer           = Row<U>;
        using reference         = Row<U>;

    private:
        Slice2D<U> * parent;
        std::size_t idx;

    public:
        constexpr Iterator(Slice2D<U> * parent, std::size_t idx) : parent(parent), idx(idx) { }
        
        constexpr operator bool() const { return parent; }

        constexpr bool operator==(Iterator<U> const& other) const { return parent == other.parent && idx == other.idx; }
        constexpr bool operator!=(Iterator<U> const& other) const { return parent != other.parent || idx != other.idx; }

        constexpr Iterator<U> & operator+=(difference_type d)       { idx += d; return *this;                  }
        constexpr Iterator<U> & operator-=(difference_type d)       { idx -= d; return *this;                  }
        constexpr Iterator<U> & operator++()                        { ++idx; return *this;                     }
        constexpr Iterator<U> & operator--()                        { --idx; return *this;                     }
        constexpr Iterator<U>   operator++(int)                     { auto other = *this; ++idx; return other; }
        constexpr Iterator<U>   operator--(int)                     { auto other = *this; --idx; return other; }
        constexpr Iterator<U>   operator+ (difference_type d) const { return Iterator<U>(parent, d + idx);     }
        constexpr Iterator<U>   operator- (difference_type d) const { return Iterator<U>(parent, d - idx);     }

        constexpr difference_type operator-(Iterator<U> const& other) const { return idx - other.idx; }

        constexpr Row<U>       operator*()       { ASSERT(idx >= 0 && idx < parent->Height()); return (*parent)[idx]; }
        constexpr Row<U const> operator*() const { ASSERT(idx >= 0 && idx < parent->Height()); return (*parent)[idx]; }
        
        constexpr Row<U>       operator->()       { ASSERT(idx >= 0 && idx < parent->Height()); return (*parent)[idx]; }
        constexpr Row<U const> operator->() const { ASSERT(idx >= 0 && idx < parent->Height()); return (*parent)[idx]; }
    };
    
public:
    using iterator               = Iterator<T>;
    using const_iterator         = Iterator<T const>;
    using reverse_iterator       = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;
    
    using value_type             = T;
    using size_type              = std::size_t;
    using difference_type        = std::size_t;
    using pointer                = typename iterator::pointer;
    using reference              = typename iterator::reference;
    using const_pointer          = typename const_iterator::pointer;
    using const_reference        = typename const_iterator::reference;
    
private:
    T * ptr;
    Shape2D shape;
    std::size_t stride;
    
public:
    constexpr Slice2D(Slice2D<T> const& other) : ptr(other.ptr), stride(other.stride), shape(other.Shape()) { }
    constexpr Slice2D(Row<T> other) : ptr(other.Data()), stride(other.Size()), shape(other.Shape()) { }

    constexpr Slice2D(T * ptr, Shape2D shape, std::size_t stride) : ptr(ptr), shape(shape), stride(stride) { }
    constexpr Slice2D(T * ptr, std::size_t w, std::size_t h, std::size_t stride) : Slice2D(ptr, Shape2D(w, h), stride) { }

    constexpr Row<T> operator[](std::size_t idx)
    {
        ASSERT(idx >= 0 && idx < shape.Height());
        return Row<T>(&ptr[idx * stride], shape.Width());
    }

    constexpr Row<T const> operator[](std::size_t idx) const
    {
        ASSERT(idx >= 0 && idx < shape.Height());
        return Row<T const>(&ptr[idx * stride], shape.Width());
    }
    
    constexpr Slice2D<T> Slice(std::size_t x, std::size_t y, Shape2D const& sh)
    {
        ASSERT(x >= 0 && x < shape.Width());
        ASSERT(y >= 0 && y < shape.Height());

        T * p = &ptr[y * stride + x];
        std::size_t w = std::min(sh.Width(),  shape.Width()  - x);
        std::size_t h = std::min(sh.Height(), shape.Height() - y);
        return Slice2D<T>(p, w, h, stride);
    }

    constexpr Slice2D<T const> Slice(std::size_t x, std::size_t y, Shape2D const& sh) const
    {
        ASSERT(x >= 0 && x < shape.Width());
        ASSERT(y >= 0 && y < shape.Height());

        T const* p = &ptr[y * stride + x];
        std::size_t w = std::min(sh.Width(),  shape.Width()  - x);
        std::size_t h = std::min(sh.Height(), shape.Height() - y);
        return Slice2D<T const>(p, w, h, stride);
    }
    
    constexpr Slice2D<T>       Slice(std::size_t x, std::size_t y, std::size_t w, std::size_t h)       { return Slice(x, y, Shape2D(w, h)); }
    constexpr Slice2D<T const> Slice(std::size_t x, std::size_t y, std::size_t w, std::size_t h) const { return Slice(x, y, Shape2D(w, h)); }
    
    constexpr Slice2D<T>       Slice()       { return *this; }
    constexpr Slice2D<T const> Slice() const { return *this; }
    
    constexpr operator Slice2D<std::remove_const_t<T>> const& () const
    {
        return *(Slice2D<std::remove_const_t<T>>*)(this);
    }
    
    constexpr operator Slice2D<T const> const& () const
    {
        return *(Slice2D<T const> const*)(this);
    }

    constexpr std::size_t Stride() const { return stride;         }
    constexpr Shape2D     Shape()  const { return shape;          }
    constexpr std::size_t Width()  const { return shape.Width();  }
    constexpr std::size_t Height() const { return shape.Height(); }
    
    constexpr T      * Data()       { return ptr; }
    constexpr T const* Data() const { return ptr; }

    constexpr Array2D<std::remove_const_t<T>> Copy() const
    {
        Array2D<std::remove_const_t<T>> copy(Width(), Height());
        *copy = *this;
        return copy;
    }
    
    constexpr Slice2D<T> operator*() { return *this; }

    constexpr Slice2D<T> & operator=(Slice2D<T> const& other) &
    {
        ptr = other.ptr;
        shape = other.shape;
        stride = other.stride;
    }

    VISIT_FUNCTIONS(Slice2D);
    BINARY_ASSIGN_OPERATORS(Slice2D);
    
    constexpr iterator               begin()         noexcept { return iterator(this, 0);                                 }
    constexpr const_iterator         begin()   const noexcept { return const_iterator((Slice2D<T const>*)this, 0);        }
    constexpr const_iterator         cbegin()  const noexcept { return const_iterator((Slice2D<T const>*)this, 0);        }
    constexpr iterator               end()           noexcept { return iterator(this, Height());                          }
    constexpr const_iterator         end()     const noexcept { return const_iterator((Slice2D<T const>*)this, Height()); }
    constexpr const_iterator         cend()    const noexcept { return const_iterator((Slice2D<T const>*)this, Height()); }
    constexpr reverse_iterator       rbegin()        noexcept { return reverse_iterator(end());                           }
    constexpr const_reverse_iterator rbegin()  const noexcept { return reverse_iterator(end());                           }
    constexpr const_reverse_iterator crbegin() const noexcept { return const_reverse_iterator(end());                     }
    constexpr reverse_iterator       rend()          noexcept { return reverse_iterator(begin());                         }
    constexpr const_reverse_iterator rend()    const noexcept { return reverse_iterator(begin());                         }
    constexpr const_reverse_iterator crend()   const noexcept { return const_reverse_iterator(begin());                   }
};
